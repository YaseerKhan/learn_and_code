package learnandcode;

import java.util.Scanner;

public class LittleMonk extends ArrayStack {

	private int index;
	private int numberOfParantheses;
	private int parantheses;
	private int prevMaxStreak, lastValidStreak, currentStreak;
	private static int maxStreak;
	private Scanner scannerObject;

	public LittleMonk(int capacity) {
		super(capacity);
	}

	private int findTheLongestSubarray() throws Exception {

		scannerObject = new Scanner(System.in);
		numberOfParantheses = scannerObject.nextInt();

		for (index = 0; index < numberOfParantheses; index++) {
			parantheses = scannerObject.nextInt();

			if (!isEmpty() && parantheses < 0 && (top() == (-1 * parantheses))) {
				pop();
				maxStreak = calculateMaxStreak(maxStreak, currentStreak);
			} else {
				push(parantheses);
				currentStreak = 0;
			}
		}
		return maxStreak;

	}

	private int calculateMaxStreak(int maxStreak, int currentStreak) {

		if (prevMaxStreak + 2 == index) {
			currentStreak = lastValidStreak + 1;
		} else {
			currentStreak++;
		}
		prevMaxStreak = index;
		lastValidStreak = currentStreak;
		maxStreak = Math.max(maxStreak, currentStreak);
		return maxStreak;

	}

	public static void main(String[] args) throws Exception {
		LittleMonk littleObject = new LittleMonk(10000);
		System.out.println(2 * littleObject.findTheLongestSubarray());
	}
}
