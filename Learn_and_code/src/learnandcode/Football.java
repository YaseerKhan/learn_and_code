package learnandcode;

import java.util.Scanner;

public class Football extends ArrayStack {

	private int backPassPlayer = 0;
	private int scenario = 0;
	private int testcase = 0;
	private int currentPlayer = 0;
	private int countBackPassSequence = 0;
	String sequence = null;

	public Football(int capacity) {
		super(capacity);
	}

	private int findBallPossesedBy() throws Exception {

		Scanner in = new Scanner(System.in);

		// Enter Over all test scenario
		scenario = in.nextInt();
		while (scenario != 0) {
			scenario--;
			// Enter no.of players
			testcase = in.nextInt();
			backPassPlayer = in.nextInt();

			while (testcase != 0) {
				testcase--;
				sequence = in.next();
				if (sequence.equals("P")) {
					// Enter player number
					currentPlayer = in.nextInt();
					push(currentPlayer);

					countBackPassSequence = 0;
				} else if (sequence.equals("B") && !isEmpty()) {
					countBackPassSequence++;
					checkBackPassSequence(countBackPassSequence, backPassPlayer);
				}
			}
		}
		return (int) peek();
	}

	private void checkBackPassSequence(int countBackPassSequence,
			int backPassPlayer) throws Exception {
		if ((countBackPassSequence % 2) == 0) {
			push(backPassPlayer);
		} else {
			backPassPlayer = (int) pop();
		}
	}

	public static void main(String[] args) throws Exception {
		Football footballObject = new Football(1000000);
		System.out.println("Player " + footballObject.findBallPossesedBy());
	}
}
