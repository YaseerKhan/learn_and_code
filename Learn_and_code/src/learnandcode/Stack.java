package learnandcode;

public interface Stack {

	public int size();

	public boolean isEmpty();

	public int top() throws Exception;

	public Object pop() throws Exception;
	
	public Object peek() throws Exception;

	void push(int element) throws Exception;
	
}