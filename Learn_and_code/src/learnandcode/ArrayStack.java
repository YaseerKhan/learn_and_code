package learnandcode;

public class ArrayStack implements Stack {
	protected int[] stack;
	protected int capacity;
	protected static int currentPos = -1;

	public ArrayStack(int capacity) {
		this.capacity = capacity;
		stack = new int[capacity];
	}

	@Override
	public int size() {
		return this.capacity;
	}

	@Override
	public boolean isEmpty() {
		if (currentPos < 0) {
			return true;
		}
		return false;
	}

	@Override
	public int top() throws Exception {
		if (isEmpty()) {
			throw new Exception(" Stack is Empty");
		}
		return (int) stack[currentPos];
	}

	@Override
	public void push(int element) throws Exception {
		if (currentPos == capacity) {
			throw new Exception();
		}
		currentPos++;
		stack[currentPos] = element;
	}

	@Override
	public Object pop() throws Exception {
		if (isEmpty()) {
			throw new Exception("Stack is Empty ");
		}
		return stack[currentPos--];
	}

	@Override
	public Object peek() throws Exception {
		return top();
	}
}
