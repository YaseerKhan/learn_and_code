package learnandcode;

import java.util.*;

public class TryLittleMonk extends MyGenericsStack<Object>{
	public TryLittleMonk(int size) {
		super(size);
	}

	int bracket, count, currentStreak, lastValidStreak, maxStreak;
	int max = 0, balancingLength = 0, waitingLength = 0,
			greatestBalancingLength = 0;

	public void findLongestSubArray() throws Exception {
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();

		for (int i = 0; i < size; i++) {
			int element = sc.nextInt();
			if (element > 0) {
				pushElement(element);
			} else {
				calculateLegth(element);

			}
		}
		System.out.println("" + greatestBalancingLength);

	}

	private void calculateLegth(int element) throws Exception {
		if ((int)peek() == Math.abs(element)) {
			popElementAndKeepCount();
		} else {
			clearStack();
			balancingLength = 0;
			waitingLength = 0;
		}

	}

	private void popElementAndKeepCount() throws Exception {
		popElement();
		if (isStackEmpty()) {
			balancingLength += 2;
		} else {
			waitingLength += 2;
			if (balancingLength < waitingLength) {
				balancingLength = waitingLength;
			}
		}
		if (balancingLength > greatestBalancingLength) {
			greatestBalancingLength = balancingLength;
		}

	}

	public static void main(String args[]) throws Exception {
		TryLittleMonk littleMonkObject = new TryLittleMonk(1000000);
		littleMonkObject.findLongestSubArray();

	}
}
