package learnandcode;

import java.util.Scanner;

import org.apache.poi.ss.formula.functions.T;

public class LittleMonk extends ArrayStack<T> {

	private int index;
	private int numberOfParantheses;
	private int input;
	private static int prevMaxStreak, lastValidStreak, currentStreak;
	private static int maxStreak;
	private Scanner scannerObject;

	public LittleMonk(int capacity) {
		super(capacity);
	}

	private int findTheLongestSubarray() throws Exception {

		scannerObject = new Scanner(System.in);
		numberOfParantheses = scannerObject.nextInt();

		for (index = 0; index < numberOfParantheses; index++) {
			input = scannerObject.nextInt();

			if (!isStackEmpty() && (input < 0) && checkTop(input)) {
				popElement();
				maxStreak = calculateMaxStreak(maxStreak, currentStreak,index);
			} else {
				pushElement(input);
				currentStreak = 0;
			}
		}
		return maxStreak;

	}

	private boolean checkTop(int input) throws Exception {
		int stackTop = Integer.class.cast(getTopValue());
		return (stackTop == input);
	}

	private int calculateMaxStreak(int maxStreak, int currentStreak, int index) {

		if (prevMaxStreak + 2 == index) {
			currentStreak = lastValidStreak + 1;
		} else {
			currentStreak++;
		}
		prevMaxStreak = index;
		lastValidStreak = currentStreak;
		maxStreak = Math.max(maxStreak, currentStreak);
		return maxStreak;

	}

	public static void main(String[] args) throws Exception {
		LittleMonk littleObject = new LittleMonk(10000);
		System.out.println(2 * littleObject.findTheLongestSubarray());
	}
}
