package learnandcode;

import java.util.Scanner;

public class SignalRange extends MyGenericsStack<Object>{

	private int range=1;
    private	Scanner in = new Scanner(System.in);

	public SignalRange(int capacity) {
		super(capacity);
	}

	private void calculateSignalRange() {
		int sampleTestCase = in.nextInt();
		while(sampleTestCase!=0)
			sampleTestCase--;
		findSampleSignalRange();
	}

	private void getSignalHeight() {
		int no_of_towers= in.nextInt();
		int input;
		for(int i=0 ; i<no_of_towers;i++){
			input=in.nextInt();
			push(input);
		}
	
	}

	private void findSampleSignalRange() {
		getSignalHeight();
		int testCase = in.nextInt();
		while (testCase!=0) {
			testCase--;
			
		}
	}

	public static void main(String[] args) {
		SignalRange signalObject= new SignalRange(1000000);
		signalObject.calculateSignalRange();
		
	}

}
