package learnandcode;

import java.util.Scanner;

public class MyGenericsStack<T extends Object> {

	private int stackSize;
	private T[] stackArray;
	private int top;

	@SuppressWarnings("unchecked")
	public MyGenericsStack(int size) {
		this.stackSize = size;
		this.stackArray = (T[]) new Object[stackSize];
		this.top = -1;
	}

	public void pushElement(T entry) {
		if (this.isStackFull()) {
			System.out.println(("Stack is full. Increasing the capacity."));
			this.increaseStackCapacity();
		}
		this.stackArray[++top] = entry;
	}

	
	public T popElement() throws Exception {
		if (this.isStackEmpty()) {
			throw new Exception("Stack is empty. Can not remove element.");
		}
		T entry = this.stackArray[top--];
		return entry;
	}

	public T peek() {
		return stackArray[top];
	}

	private void increaseStackCapacity() {

		@SuppressWarnings("unchecked")
		T[] newStack = (T[]) new Object[this.stackSize * 2];
		for (int i = 0; i < stackSize; i++) {
			newStack[i] = this.stackArray[i];
		}
		this.stackArray = newStack;
		this.stackSize = this.stackSize * 2;
	}

	public boolean isStackEmpty() {
		return (top == -1);
	}

	public boolean isStackFull() {
		return (top == stackSize - 1);
	}

	public void clearStack() {

		for (int i = 0; i < top; i++)
			stackArray[i] = null;

		top = -1;
	}
}
