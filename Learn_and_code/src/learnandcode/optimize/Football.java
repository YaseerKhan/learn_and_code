package learnandcode;

import java.util.Scanner;
public class Football extends ArrayStack<Object> {

	private int backPassPlayer = 0;
	private int scenario = 0;
	private int testcase = 0;
	private int currentPlayer = 0;
	private int countBackPassSequence = 0;
	private String sequence = null;
	private Scanner in = new Scanner(System.in);

	public Football(int capacity) {
		super(capacity);
	}

	private int findBallPossesedBy() throws Exception {

		// Enter Over all test scenario
		scenario = in.nextInt();
		while (scenario != 0) {
			scenario--;
			// Enter no.of players
			testcase = in.nextInt();
			backPassPlayer = in.nextInt();

			whoHasTheBall();
		}
		return Integer.class.cast(peek());
	}

	private void whoHasTheBall() {
		while (testcase != 0) {
			testcase--;
			sequence = in.next();
			if (sequence.equals("P")) {
				// Enter player number
				currentPlayer = in.nextInt();
				pushElement(currentPlayer);

				countBackPassSequence = 0;
			} else if (sequence.equals("B") && !isStackEmpty()) {
				countBackPassSequence++;
				checkBackPassSequence(countBackPassSequence, backPassPlayer);
			}
		}
		
	}

	private void checkBackPassSequence(int countBackPassSequence,int backPassPlayer) throws Exception {
		if ((countBackPassSequence % 2) == 0) {
			pushElement(backPassPlayer);
		} else {
			backPassPlayer = Integer.class.cast(getpopElement());
		}
	}

	public static void main(String[] args) throws Exception {
		Football footballObject = new Football(1000000);
		System.out.println("Player " + footballObject.findBallPossesedBy());
	}
}