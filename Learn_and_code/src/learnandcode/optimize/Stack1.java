package learnandcode;

import org.apache.poi.ss.formula.functions.T;

public interface Stack1 {

	public int size();

	public boolean isStackEmpty();

	public boolean isStackFull();

	public T getTopValue() throws Exception;

	public void popElement() throws Exception;
	
	public T peek() throws Exception;

	public void pushElement(T element) throws Exception;

	public T getpopElement() throws Exception;
	
}