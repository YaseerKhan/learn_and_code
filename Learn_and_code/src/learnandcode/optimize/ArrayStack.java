package learnandcode;


public class ArrayStack<T extends Object> implements Stack1 {

	protected T[] stack;
	protected int capacity;
	protected int currentPos;
	protected T popedElement;
	
	@SuppressWarnings("unchecked")
	public ArrayStack(int capacity) {
		this.capacity = capacity;
		this.stack = (T[]) new Object[capacity];
		this.currentPos = -1;
	}

	@Override
	public int size() {
		return this.capacity;
	}

	@Override
	public boolean isStackEmpty() {
		return (currentPos == -1);
	}

	@Override
	public boolean isStackFull() {
		return (currentPos == capacity + 1);
	}

	@Override
	public org.apache.poi.ss.formula.functions.T getTopValue() throws Exception {
		if (isStackEmpty()) {
			throw new Exception("Stack is empty. Can not remove element.");
		}
		return (org.apache.poi.ss.formula.functions.T) stack[currentPos];
	}

	@Override
	public void popElement() throws Exception {
		if (isStackEmpty()) {
			throw new Exception("Stack is empty. Can not remove element.");
		}
		currentPos--;
		popedElement = stack[currentPos];
	}

	@Override
	public org.apache.poi.ss.formula.functions.T getpopElement() throws Exception {

		return (org.apache.poi.ss.formula.functions.T) popedElement;
	}

	@Override
	public org.apache.poi.ss.formula.functions.T peek() throws Exception {
		if (isStackEmpty()) {
			throw new Exception("Stack is empty. Can not remove element.");
		}
		return (org.apache.poi.ss.formula.functions.T) stack[currentPos];
	}

	@SuppressWarnings("unchecked")
	@Override
	public void pushElement(org.apache.poi.ss.formula.functions.T element)
			throws Exception {
		if (this.isStackFull()) {
			System.out.println(("Stack is full. Increasing the capacity."));
		}
		currentPos++;
		this.stack[currentPos] = (T) element;
	}

}
