package com.hp.ipg.devices.mocks.yasser;

import java.util.Scanner;

class Stack {
	private static final int capacity = 100000;
	public int arr[] = new int[capacity];
	protected int top = -1;

	public Stack() {

	}

	public void push(int pushElement) {
		if (top < capacity - 1) {
			top++;
			arr[top] = pushElement;
		} else {
			System.out.println("Stack Overflow !");
		}
	}

	public void pop() {
		if (top >= 0) {
			top--;
		} else {
			System.out.println("Stack Underflow !");
		}
	}

	public boolean empty() {
		if (top < 0) {
			return false;
		}
		return true;
	}
}

class LittleMonk {
	private int index;
	private Stack stackObject= new Stack();

	private int numberOfParantheses, parantheses;
	private int prevMaxStreak, lastValidStreak, streak;
	private static int maxStreak;

	private int findTheLongestSubArray() {

		Scanner s = new Scanner(System.in);
		numberOfParantheses = s.nextInt();

		for (index = 0; index < numberOfParantheses; index++) {
			parantheses = s.nextInt();

			if (stackObject.empty() && parantheses < 0
					&& stackObject.arr[stackObject.top] == (-1 * parantheses)) {
				stackObject.pop();
				if (prevMaxStreak + 2 == index) {
					streak = lastValidStreak + 1;
				} else {
					streak++;
				}
				prevMaxStreak = index;
				lastValidStreak = streak;
				maxStreak = Math.max(maxStreak, streak);
			} else {
				stackObject.push(parantheses);
				streak = 0;
			}
		}
		return maxStreak;

	}

	public static void main(String[] args) {

		LittleMonk littleObject = new LittleMonk();
		System.out.println(2 * littleObject.findTheLongestSubArray());
	}
}

